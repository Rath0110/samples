﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ObjectFileParser.Test
{
    [TestClass]
    public class ParseTest
    {
        [TestMethod]
        public void AnalyzeLinesTestVertices()
        {
            var parser = new Loader(@"Resources\Castle\castle01.obj");
            parser.Load();
            parser.AnalyzeLines();
            Assert.AreEqual(84343, parser.Results["v"].Count);
        }
    }
}
