﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectFileParser
{
    using System.Collections.ObjectModel;
    using System.IO;

    public class Loader
    {
        public string[] Lines { get; set; }
        public string AbsolutePath { get; set; }
        public Dictionary<string, List<string>> Results;

        private static string[] keys = new string[10]{ "mtllib","vn","vt","vs","v","o","g","s","usemtl","f"};

        public Loader()
        {
            Results = new Dictionary<string, List<string>>();
            foreach(var key in keys)
            {
                var coll = new List<string>();
                Results.Add(key, coll);
            }
        }

        public Loader(string path)
        {
            AbsolutePath = path;
            Results = new Dictionary<string, List<string>>();
            foreach (var key in keys)
            {
                var coll = new List<string>();
                Results.Add(key, coll);
            }
        }

        public void Load()
        {
            //Make sure we have a file path
            if (!string.IsNullOrEmpty(AbsolutePath))
            {
                //Make sure the file exists
                if (File.Exists(AbsolutePath))
                {
                    Lines = File.ReadAllLines(AbsolutePath);
                }
            }
        }

        public void AnalyzeLines()
        {
            //Only parse if there are lines loaded
            if(Lines.Length > 0)
            {
                //Use normal For Loops to maintain line order
                //Loop through each line
                for(var x = 0; x < Lines.Length; x++)
                {
                    //Loop through each key examining the line
                    for(var y = 0; y < keys.Length; y++)
                    {
                        if (Lines[x].Contains(keys[y]))
                        {
                            Results[keys[y]].Add(Lines[x]);
                        }
                    }
                }
            }
        }       
    }
}
