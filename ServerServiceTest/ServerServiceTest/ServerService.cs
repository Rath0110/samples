﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ServiceProcess;

namespace ServerServiceTest
{
    class ServerService : System.ServiceProcess.ServiceBase
    {
        //Main Process Entry Point
        static void Main()
        {
            var ServicesToRun = new ServiceBase[] { new ServerService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        private void InitializeComponent()
        {
            this.ServiceName = "Test Server Service";
        }
        private string folderPath = @"c:\temp";

        protected override void OnStart(string[] args)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);   
            }

            var fs = new FileStream(folderPath + "\\ServerService.txt",FileMode.OpenOrCreate, FileAccess.Write);
            var m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            m_streamWriter.WriteLine(" Server Service Started at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "\n");
            m_streamWriter.Flush();
            m_streamWriter.Close();
        }

        protected override void OnStop()
        {
            var fs = new FileStream(folderPath + "\\ServerService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            var m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            m_streamWriter.WriteLine(" Server Service Stopped at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "\n");
            m_streamWriter.Flush();
            m_streamWriter.Close();
        }
    }
}
