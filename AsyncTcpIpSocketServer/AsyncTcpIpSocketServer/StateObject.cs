﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace AsyncTcpIpSocketServer
{
    public class StateObject
    {
        //Client Socket
        public Socket workSocket = null;
        //Size of Receive Buffer
        public const int BufferSize = 1024;
        //Receive Buffer
        public byte[] buffer = new byte[BufferSize];
        //Received Data String
        public StringBuilder sb = new StringBuilder();
    }

    public class AsynchronousSocketListener
    {
        //Thread signal
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public AsynchronousSocketListener()
        {

        }

        public static void StartListening()
        {
            //Data Buffer for incoming data
            var bytes = new Byte[1024];

            //Establish the local endpoint for the socket.
            //The DNS name of the computer
            //running the listener is "host.contoso.com"
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[2];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 12000);

            //Create a TCP/IP Socket
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Bind the socket tothe local end point and listen for incoming connections
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    //Set the event to nonsignaled state
                    allDone.Reset();

                    //start an aynchronous socket to listen for connections.
                    Console.WriteLine("Waiting for a connection....");
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    //Wait until a connectio is made before continuing.
                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue....");
            Console.Read();
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            //Signal the main thread to continue.
            allDone.Set();

            //Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            //Create the state object
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            //Retrieve the state obejct and the handler socket from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            //Read the data from the client socket
            int bytesRead = handler.EndReceive(ar);

            if(bytesRead > 0)
            {
                //there might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                //check for the end-of-file tag. if it is not there, read more data.
                content = state.sb.ToString();
                if(content.IndexOf("<EOF>") > -1)
                {
                    //All the data has been read from the client. Display it on the console.
                    Console.WriteLine("Read {0} bytes from socket. \n Data: {1}", content.Length, content);
                    //Echo the data back to the client.
                    Send(handler, content);
                }
                else
                {
                    //Not all data received. get more
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private static void Send(Socket handler, String data)
        {
            //convert the string data to byte data using ASCII encoding
            var byteData = Encoding.ASCII.GetBytes(data);

            //Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                //Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                //complete sending the data to the remote device
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to the client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            StartListening();
            return 0;
        }
    }
}
